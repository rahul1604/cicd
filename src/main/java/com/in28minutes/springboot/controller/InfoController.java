package com.in28minutes.springboot.controller;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import com.in28minutes.springboot.security.SecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.in28minutes.springboot.model.Course;
import com.in28minutes.springboot.service.StudentService;

@RestController
public class InfoController {


    @GetMapping("/info")
    public Info info() {
        return new Info(SecurityConfig.deploymentTime, LocalDateTime.now(), "Service_Inatance_1");
    }
}
