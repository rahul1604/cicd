package com.in28minutes.springboot.controller;

import java.time.LocalDateTime;

public class Info {
    public LocalDateTime deploymentTime;
    public LocalDateTime currentServerTime;
    public String serviceName;

    public Info(LocalDateTime deploymentTime, LocalDateTime currentServerTime, String serviceName) {
        this.deploymentTime = deploymentTime;
        this.currentServerTime = currentServerTime;
        this.serviceName = serviceName;
    }
}
