package com.in28minutes.springboot.service;

import org.junit.Test;

import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.*;

public class StudentServiceTest {

    public static final Logger LOG = Logger.getLogger(StudentServiceTest.class.getName());

    @Test
    public void checkIfStudentsStoreInDb() {
        Thread[] threadArray = new Thread[50];
        LOG.log(Level.INFO, String.format("Storing %s number of students into DB", threadArray.length));
        for (int i = 0; i < threadArray.length; i++) {
            final int k = i;
            LOG.log(Level.INFO, String.format("Creating Students of Id= %s which will be stored in db", k));
            threadArray[i] = new Thread(() -> {
                LOG.log(Level.INFO, String.format("Storing Students of Id= %s into in db", k));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    LOG.log(Level.SEVERE, String.format("Error occured while storing Students into db"));
                }
            });
        }

        for (int i = 0; i < threadArray.length; i++) {
            threadArray[i].start();
        }

    }
}