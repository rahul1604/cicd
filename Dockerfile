FROM openjdk:8u121-jdk-alpine
  
# Set a directory for the app

WORKDIR /app

EXPOSE 8083

ENV PORT 8083

# copy jar files to the container
COPY ./target/*.jar .

RUN mv *.jar service.jar

ENTRYPOINT [ "/usr/bin/java" ]

# run the command
CMD ["-jar", "./service.jar", "-service.port=${PORT}"]
