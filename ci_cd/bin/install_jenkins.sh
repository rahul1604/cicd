install_java() {
  # install check
  sudo apt install openjdk-8-jdk

  # check if java is installaed or not?
  java -version
}

install_setup_jenkins() {
  # add the repository key to the system
  wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -

  # append the Debian package repository address to the server
  sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'

  # When both of these are in place, run update so that apt will use the new repository:
  sudo apt update

  #  install Jenkins and its dependencies:
  sudo apt install jenkins

  # start Jenkins using systemctl:
  sudo systemctl start jenkins

  # ou can use its status command to verify that Jenkins started successfully:
  sudo systemctl status jenkins

  # By default, Jenkins runs on port 8080, so open that port using ufw:
  sudo ufw allow 8080

  # Check the status to confirm the new rules:
  sudo ufw status
}

install_java
install_setup_jenkins
