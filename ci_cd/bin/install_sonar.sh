
# https://www.howtoforge.com/how-to-install-sonarqube-on-ubuntu-1804/

pre_setup() {
  sudo apt-get update -y
}

setup_java() {
  sudo add-apt-repository ppa:webupd8team/java
  sudo apt-get update -y
  sudo apt-get install oracle-java8-installer -y
  java -version
}

setup_postgres() {
  sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
  wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | sudo apt-key add -
  sudo apt-get update -y
  sudo apt-get install postgresql postgresql-contrib
  sudo systemctl status postgresql
  su - postgres
  createuser sonar
  psql
  ALTER USER sonar WITH ENCRYPTED password 'password';
  CREATE DATABASE sonar OWNER sonar;
}

setup_sonar() {
  sudo adduser sonar
  wget https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-6.7.6.zip
  unzip sonarqube-6.7.6.zip
  sudo cp -r sonarqube-6.7.6 /opt/sonarqube
  sudo chown -R sonar:sonar /opt/sonarqube
  sudo nano /opt/sonarqube/bin/linux-x86-64/sonar.sh
  RUN_AS_USER=sonar
  sudo nano /opt/sonarqube/conf/sonar.properties
  sonar.jdbc.username=sonar
  sonar.jdbc.password=password
  sonar.jdbc.url=jdbc:postgresql://localhost/sonar
  sonar.web.host=127.0.0.1
  sonar.search.javaOpts=-Xms512m  -Xmx512m
}

add_sonar_to_systemd() {
sudo nano /etc/systemd/system/sonar.service
cat <<EOF
[Unit]
Description=SonarQube service
After=syslog.target network.target

[Service]
Type=forking

ExecStart=/opt/sonarqube/bin/linux-x86-64/sonar.sh start
ExecStop=/opt/sonarqube/bin/linux-x86-64/sonar.sh stop

User=sonar
Group=sonar
Restart=always

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl start sonar
sudo systemctl enable sonar
sudo systemctl status sonar

}

setup_apache() {
  sudo apt-get install apache2 -y
sudo a2enmod proxy
sudo a2enmod proxy_http
sudo nano /etc/apache2/sites-available/sonar.conf

cat <<EOF
<VirtualHost *:80>
    ServerName example.com
    ServerAdmin admin@example.com
    ProxyPreserveHost On
    ProxyPass / http://127.0.0.1:9000/
    ProxyPassReverse / http://127.0.0.1:9000/
    TransferLog /var/log/apache2/sonarm_access.log
    ErrorLog /var/log/apache2/sonar_error.log
</VirtualHost>
EOF

sudo a2ensite sonar
sudo systemctl restart apache2
sudo systemctl restart sonar
sudo tail -f /opt/sonarqube/logs/sonar.log
sudo tail -f /opt/sonarqube/logs/web.log

}