
install_docker() {
   # update your existing list of packages:
  sudo apt update

  # install a few prerequisite packages which let apt use packages over HTTPS:
  sudo apt install apt-transport-https ca-certificates curl software-properties-common

  # add the GPG key for the official Docker repository to your system:
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

  # Add the Docker repository to APT sources:
  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"

  # update the package database with the Docker packages from the newly added repo:
  sudo apt update

  # Make sure you are about to install from the Docker repo instead of the default Ubuntu repo:
  apt-cache policy docker-ce

  # install Docker:
  sudo apt install docker-ce

  # Docker should now be installed, the daemon started, and the process enabled to sudo apt updatestart on boot.
  sudo systemctl status docker

  # if error occureed for the above command use sudo /etc/init.d/docker start
  # sudo nohup docker daemon -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock
}



execute_docker_cmd_withoud_sudo() {
  #export USER=dtripathy
  # If you want to avoid typing sudo whenever you run the docker command, add your username to the docker group:
  #export USER=jenkins
  sudo usermod -aG docker ${USER}
  sudo chmod 777 /var/run/docker.sock

  # To apply the new group membership, log out of the server and back in, or type the following:
  su - ${USER}

  # Confirm that your user is now added to the docker group by typing:
  id -nG
}

docker_command() {
  # To view system-wide information about Docker, use:
  docker info

  # download a image
  docker pull busybox
  # you can use the docker images command to see a list of all images on your system.
  docker images
  # run a image
  docker run busybox
  docker run busybox echo "hello from busybox"
  docker ps
  docker ps -a
  docker run -it busybox sh
  docker rm 305297d7a235 ff0a5c3750b9

  #  -it: to enable Interactive Session/SSH to get into the container at a later point in time
  #  -d: Run the container in the background (always recommended)
  #  --name: name your container
  #  saravak/tomcat8: the Image used to create this container. the Image instantiated as a container
  # -p 8081:8080:  Forwarding the Container port 8080 to Host 8081


  docker pull jenkins/jenkins


  docker run -it -d -p 80:80 -v jenkins_home:/var/jenkins_home jenkins/jenkins:lts

  @ To get the running instance id
  docker ps
  # enter into a running docker instance
  docker exec -it 7b1e2d443732 bash

  # netstat -tulpn | grep LISTEN
  To check if port is running or not


  # docker image build
  docker build -t jenkins_demo_image .
}

install_docker
execute_docker_cmd_withoud_sudo
