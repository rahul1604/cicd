install_java() {
  # install check
  sudo apt install openjdk-8-jdk

  # check if java is installaed or not?
  java -version
}

install_setup() {

  # https://devopscube.com/how-to-install-latest-sonatype-nexus-3-on-linux/

  # Create a directory named app and cd into the directory.
  sudo mkdir /app && cd /app

  # Download the latest nexus.
  sudo wget -O nexus.tar.gz https://download.sonatype.com/nexus/3/latest-unix.tar.gz

  # Untar the downloaded file.
  sudo tar -xvf nexus.tar.gz

  # Rename the untared file to nexus.
  sudo mv nexus-3* nexus

  # As a good security practice, it is not advised to run nexus service with any sudo user. So create a new user named nexus.
  sudo adduser nexus

  # Change the ownership of nexus files and nexus data directory to nexus user.
  sudo chown -R nexus:nexus /app/nexus
  sudo chown -R nexus:nexus /app/sonatype-work

  sudo vi  /app/nexus/bin/nexus.rc

  run_as_user="nexus"

  sudo vi /app/nexus/bin/nexus.vmoptions

 cat <<EOF
 -Xms2703m
-Xmx2703m
-XX:MaxDirectMemorySize=2703m
-XX:+UnlockDiagnosticVMOptions
-XX:+UnsyncloadClass
-XX:+LogVMOutput
-XX:LogFile=../sonatype-work/nexus3/log/jvm.log
-XX:-OmitStackTraceInFastThrow
-Djava.net.preferIPv4Stack=true
-Dkaraf.home=.
-Dkaraf.base=.
-Dkaraf.etc=etc/karaf
-Djava.util.logging.config.file=etc/karaf/java.util.logging.properties
-Dkaraf.data=/nexus/nexus-data
-Djava.io.tmpdir=../sonatype-work/nexus3/tmp
-Dkaraf.startLocalConsole=false
EOF

sudo vi /etc/systemd/system/nexus.service

cat <<EOF
[Unit]
Description=nexus service
After=network.target

[Service]
Type=forking
LimitNOFILE=65536
User=nexus
Group=nexus
ExecStart=/app/nexus/bin/nexus start
ExecStop=/app/nexus/bin/nexus stop
User=nexus
Restart=on-abort

[Install]
WantedBy=multi-user.target
EOF

#sudo chkconfig nexus on
sudo systemctl start nexus
}
